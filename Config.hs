-- | This is the configuration file, which specifies
-- what servers the bot joins, the prefix for commands,
-- and what plugins are enabled
module Config (Config(..),config) where
import Data.Set
import Infinity.Core
import Infinity.Plugins
-- After this point, you should include any plugins you want
-- to have included into the bot. Naturally, you can reconfigure
-- this at runtime.
import Infinity.Plugins.HelloWorld
import Infinity.Plugins.StateTest
import Infinity.Plugins.Fortune
import Infinity.Plugins.System
import Infinity.Plugins.Unlambda

-- | Configuration data type, specifies command
-- prefixes and all servers that're connected to
data Config = Config {
  commandPrefixes :: String,   -- ^ command prefixes, i.e. @[\'>\',\'@\',\'?\']@
  servers         :: [Server], -- ^ list of 'Server'\'s to connect to.
  enabledplugins  :: [IModule] -- ^ List of enabled plugins
}

-- | Describes a single IRC Server, you can just follow the
-- same template. To see the full datatype, check Infinity/Core.hs
freenode = Server {
  address        = "irc.freenode.org",
  port           = 6667,
  channels       = fromList ["#bot-battle-royale"],
  nickname       = "infinitybot",
  password       = "",
  realname       = "time copper",
  administrators = fromList ["thoughtpolice"]
}


config = Config {
  commandPrefixes = ['?','@','>'],
  servers         = [freenode],
  enabledplugins  = 
      [ -- Put the desired plugins' data constructors 
        -- in the list below
        mkplugin System
      , mkplugin HelloWorld
      , mkplugin StateTest
      , mkplugin Fortune
      , mkplugin Unlambda
      ]
}
