{-# OPTIONS_HADDOCK ignore-exports, prune #-}
#ifdef STATIC_BUILD
-- | The bot's brain.
-- NOTE: This is the static build of infinity,
-- documentation for imain and imain' has not
-- been generated
#else
-- | The bot's brain.
-- NOTE: This is the dynamic build of infinity,
-- documentation for staticmain has not been
-- generated.
#endif
module Infinity.Main (
#ifndef STATIC_BUILD
 imain,imain',
#else
 staticmain,
#endif
 offlinemode
) where
import qualified Network.IRC as IRC
import qualified Data.Map as M
import qualified Data.Set as S
import qualified Config as C
import System.Console.Readline
import Control.Concurrent.STM
import Control.Concurrent
import Network.Socket
#ifndef STATIC_BUILD
import System.Plugins
#endif
import Control.Monad
import Text.Printf
import System.Exit
import System.IO
import Data.List
import Network
-- infinity-related stuff
import Infinity.Plugins
import Infinity.State
import Infinity.Util
import Infinity.Core
import Infinity.IRC
import Infinity.Log

#ifndef STATIC_BUILD
-- | Reboot data type
type Reboot   = (Module -> Bot -> IO ())
#endif

-- | Data that can go over the remote channel
data RemoteLine 
    = Str String              -- ^ A regular string
    | Quit (Server,Handle)    -- ^ A quit message from a server
    | Join (Server,Channel)   -- ^ Joined a channel
    | Part (Server,Channel)   -- ^ Parted the channel
    | Reboot                  -- ^ Reboot message sent
    | Nil                     -- ^ Signifies thread death, 
                              --   only happens after reboot
 deriving (Eq,Show)


-- | The bot\'s static entry point; used
-- when not built with -fdynamic
staticmain :: IO ()
staticmain = do
  -- connect to all servers
  bot     <- startup
  rchan   <- newChan
  logs Normal "Initializing plugins..."
  plugins <- initplugins (C.enabledplugins C.config)

  -- setup static servers
  let Bot x = bot
  mapM_ setup (M.toList x)
  mapM_ (forkIO . listener plugins rchan Nothing) (M.toList x)
  let bot'  = joinservs (M.toList x) bot
  logs Normal "Joined channels and identified..."
  monitor plugins rchan undefined bot'
  return ()

#ifndef STATIC_BUILD
-- | Bot\'s FIRST dynamic entry point
imain :: Module -> Reboot -> IO ()
imain mod reboot = imain' mod reboot newbot

-- | The bot\'s dynamic entry point that we jump to from 'imain'
imain' :: Module -> Reboot -> Bot -> IO ()
imain' mod reboot bot = do
  logs Normal "Initializing plugins..."
  plugins   <- initplugins (C.enabledplugins C.config)
  rchan     <- newChan :: IO (Chan RemoteLine)
  rebootvar <- atomically $ newEmptyTMVar -- when a reboot is sent, this is set

  -- we connect to any newly added servers
  let newservs = filter (not . servexists bot) (C.servers C.config)
  servs' <- mapM servcon newservs
  mapM_ setup servs'

  -- set up all new servers
  let bot'  = joinservs servs' bot
      Bot x = bot'
  mapM_ (forkIO . listener plugins rchan (Just rebootvar)) (M.toList x)
  logs Normal "Joined channels & Identified..."
  b' <- monitor plugins rchan rebootvar bot'
  reboot mod b'
#endif

-- | If infinity is built and the '-offline' command is specified,
-- then it will jump here and it will be a console interface.
offlinemode :: IO ()
offlinemode = do
  x <- initplugins (C.enabledplugins C.config)
  shell x $ IContext Nothing x Nothing
      where shell plugs ctx = do 
                s <- readline "> "
                case s of
                  Nothing     -> shell plugs ctx
                  Just "quit" -> exitWith ExitSuccess
                  Just s'     -> do 
                          addHistory s'
                          let (cmd,av) = span (/=' ') s'
                              av' = if null av then Nothing
                                     else Just (drop 1 av)
                          (str,x') <- runplugin ctx plugs cmd av'
                          putStrLn str >> shell x' ctx

-- | This is the bot's main loop. After it gets rebooted or starts
-- for the first time, it initializes all plugins, spawns a thread
-- for each server, and then jumps here. This monitors when threads
-- quit, it waits for them to join, serializes plugin state, and
-- other tasks.
monitor :: [IModule] -> Chan RemoteLine -> TMVar Bool -> Bot -> IO Bot
monitor plugins rchan rebootvar bot = do
  mlog Normal "Server's connected, threads forked..."
  loop bot
      where check b    = when (servnum b == 0) $ do 
                           mlog Normal "All servers disconnected, quitting..."
                           exitWith ExitSuccess
            wait n s i = when (i < n) $ 
                         case s of
                           Nil:xs -> wait n xs $! i+1
                           _:xs   -> wait n xs i
            loop b     = do
              check b
              l <- readChan rchan
              case l of
                Str s   -> putStrLn s >> loop b
                Quit (s,h) -> do
                           hClose h
                           loop $! (partserv s b)
                Join (s,c) -> let x = updateserv (joinchan c s) b in loop $! x 
                Part (s,c) -> let x = updateserv (joinchan c s) b in loop $! x
#ifdef STATIC_BUILD
                Reboot -> loop b
#else
                Reboot -> do
                  mlog Normal "Got Reboot message"
                  atomically $ putTMVar rebootvar True
                  s <- getChanContents rchan
                  wait (servnum b) s 0
                  return b
#endif

-- | This is the entry point for threads that listen on sockets for
-- messages
listener :: [IModule] -> Chan RemoteLine -> Maybe (TMVar Bool) -> (Server,Handle) -> IO ()
listener plugins rchan rebootvar x@(serv,handle) = infinity $ do
  str <- hGetLine handle
  writeChan rchan (Str str)
  if ping str then pong handle str
   else eval (ircParser str)
 where eval s | (Err e)            <- s = logs Error ("eval err: "++e)
              | (Line u c x)       <- s = irclog (address serv) (u,c,x)
              | (Cmd u c (cmd,av)) <- s = do
                  let cmd' = tail cmd
                  case av of
                    Nothing   -> irclog (address serv) (u,c,cmd)
                    Just args -> irclog (address serv) (u,c,unwords [cmd,args])
                  unless (null cmd') (parseCmds u cmd' av c $ IContext (Just serv) plugins (Just handle))
       parseCmds u c av chan ctx
           | "join"   == c = when (isadmin u serv) $ case av of
                               Just av' -> joinC handle av' >> writeChan rchan (Join (serv,av'))
                               Nothing  -> privmsg handle chan "Need a channel to join..."
           | "part"   == c = when (isadmin u serv) $ case av of
                               Just av' -> partC handle av' >> writeChan rchan (Part (serv,av'))
                               Nothing  -> partC handle chan >> writeChan rchan (Part (serv,chan))
--         | "quit"   == c = writeChan rchan (Quit (serv,handle))
--         | "reboot" == c = writeChan rchan Reboot
           | otherwise     = do 
              (str,plugins') <- runplugin ctx plugins c av
#ifdef DEBUG
              logs Normal $ "Output of \'"++cmd'++"\', with args \'"++(show av)++"\':   "++str
#endif
              mapM_ (\s -> privmsg handle chan s >>
                           irclog (address serv) ((nickname serv),chan,s)) (lines str)


--------------------------------
-- Utilities
--------------------------------
-- | Loops a function unless 'b' is False
infinity :: IO () -> IO ()
infinity a = a >> infinity a 

-- | Sends a message over a Handle
sendH :: Handle -> String -> IO ()
sendH h = hPrintf h "%s\r\n"

-- | Sends a private message over a handle
privmsg :: Handle -> String -> String -> IO ()
privmsg h c s = sendH h $ IRC.encode (IRC.privmsg c s)

-- | Joins a channel by sending the message over the handle
joinC :: Handle -> String -> IO ()
joinC h = sendH h . IRC.encode . IRC.joinChan 

-- | Parts a channel by sending the message over the handle
partC :: Handle -> String -> IO ()
partC h = sendH h . IRC.encode . IRC.part

-- | Check if a message is a PING
ping :: String -> Bool
ping = isPrefixOf "PING :"

-- | Send a pong message given a ping message
-- previously
pong :: Handle -> String -> IO ()
pong h s = sendH h $ "PONG " ++ (drop 5 s)

-- | our parser for irc msgs.
ircParser = parseIRCmsg (C.commandPrefixes C.config)

-- | Starts up the bot with initial configuration and
-- | whatnot, used by staticmain
startup = do
  hSetBuffering stdout NoBuffering
  logs Normal "Connecting to servers..."
  servs <- mapM servcon (C.servers C.config)
    -- add these new servers to bot state
  let bot = joinservs servs newbot
  logs Normal "Connected to servers..."
  return bot

-- | Connects to a server
servcon s = do
  let name  = address s
      portn = port s
  h <- connectTo name (PortNumber $ fromIntegral portn)
  hSetBuffering h NoBuffering
  return (s,h)


-- | Sets up a newly created server connection,
-- by sending the User, nick, and password messages
-- as well as joining channels.
setup x@(serv,handle) = do
  -- setup authentication and stuff
  sendstr handle (IRC.encode $ IRC.nick (nickname serv))                                -- nick
  sendstr handle (IRC.encode $ IRC.user (nickname serv) "0" "*" (realname serv))        -- realname
  when (not . null $ (password serv)) $ do
    sendstr handle (IRC.encode $ IRC.privmsg "nickserv" ("identify "++(password serv))) -- password
  mapM_ (sendstr handle . IRC.encode . IRC.joinChan) (S.toList $ channels serv)         -- channels
  return ()

-- | Send a string over handle
sendstr h str = hPrintf h "%s\r\n" str
