module Infinity.IRC (
 IrcLine(..),
 parseIRCmsg  -- :: String  -> String -> IrcLine
) where
import qualified Network.IRC as IRC
import Infinity.Util
import Data.List

-- | This represents the different types of information
-- that can come from IRC over the wire.
data IrcLine = Cmd  User Channel (Command,Maybe String) -- ^ A command sent over the line
             | Line User Channel String                 -- ^ A normal line of little significance
             | Err String                               -- ^ An error occured in parsing.
 deriving (Eq,Show)


-- | The IRC line parser. It reads a line over
-- the wire and then translates it into an appropriate
-- 'IrcLine'.
parseIRCmsg :: String -> String -> IrcLine
parseIRCmsg cprefix s = 
    let s' = if (last s) == '\n' then s else (s++"\n") in
    case IRC.decode s' of
      Nothing -> Err $ "irc parser err: "++s'
      Just x -> let (IRC.Message prefix _ (to:str)) = x
                    nick = getnick prefix
                    str' = concat str in
                if not (null str') then
                    if (head str') `elem` cprefix then
                        let cmd  = (head . words) str'
                            args = (tail . words) str'
                            av = if null args then
                                  Nothing else Just (unwords args) in
                        (Cmd nick to (cmd,av))
                     else (Line nick to str')
                 else (Line nick to str')
      where 
        getnick Nothing    = "nobody"
        getnick (Just pre) | (IRC.Server sn)      <- pre = sn
                           | (IRC.NickName n _ _) <- pre = n
        getnick _          = "nobody"
