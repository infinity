-- | The 'Plugin' type class that all plugins must be instances of.
-- This also includes the 'IPluginT' type which is the monad that
-- Plugins execute in the context of. This also contains many 
-- utility functions our plugins can use safely.
module Infinity.Plugins (
 -- * Types
 IModule(..),
 IContext(..),
 IPluginT,
 Plugin(..),
 -- $pfuncs
 ircPrivmsg,
 runProc, runUtil,
 pRunHelp,
 pListHelp,
 -- $plugactions
 findplugin,
 runplugin, saveplugin,
 runhelp, initplugins,
 runlist,
 -- * Miscellaneous
 mkplugin,
 ver
) where
import Control.Concurrent.STM
import Control.Monad.Reader
import qualified Control.Exception as E
import System.FilePath
import System.Timeout
import Data.Version
import System.Info
import qualified Network.IRC as IRC
import Infinity.Core (Server)
import Infinity.State
import Infinity.Util
import Infinity.Log
import Text.Printf
import Data.Binary
import System.IO
import Data.List

-- | An existential for hiding plugins
data IModule = forall p s. (Show p, Plugin p s) => IModule (p,s,TMVar Bool)


-- | This is the current context the plugin will run in; we need
-- it so we know exactly what handle to send data to, etc.
data IContext = IContext {
      currentserv :: Maybe Server,
      plugs       :: [IModule],
      handle      :: Maybe Handle
}

-- | Our Monad that all plugins execute in the context of. Our
-- internal Reader state is that of 'IContext'
newtype IPluginT a = IPluginT { runPlugT :: ReaderT IContext IO a }
    deriving (Monad,Functor,MonadIO, MonadReader IContext)


-- | This class describes all plugins, and all plugins
-- should be an instance of this class.
-- It should be noted that in the instances of this class,
-- the polymorphic type variable 'a' is just a phantom type.
-- and for most purposes can be ignored.
-- You could however, make a multi-constructor data type
-- and put in these different constructors into the list of
-- plugins (Config.hs,) and therefore have one plugin but handle different
-- or similar commands, etc by writing instances corresponding
-- to each constructor
-- But that might be silly.
class (Binary st, Show st) => Plugin a st | a -> st where
    -- | Should this plugin have state serialized?
    -- * NOTE: NIH
    serial :: a -> Bool
    -- | Give an initial state.
    pinit  :: a -> st
    -- | List of commands this plugin offers
    cmds   :: a -> [String]
    -- | Help for any one command
    help   :: a -> String -> String
    -- | Execution function
    exec   :: a                     -- ^ Phantom
           -> st                    -- ^ State
           -> String                -- ^ Command
           -> Maybe String          -- ^ Argument
           -> IPluginT (st,String)  -- ^ Return string

    serial _ = False

-- $pfunc
-- These are functions that are available to plugins
-- in their 'exec' function.

-- | Sends a privmsg to a user/channel
ircPrivmsg :: String -- ^ User/channel
           -> String -- ^ String
           -> IPluginT ()
ircPrivmsg to str = do
  h <- asks handle
  case h of
    Nothing -> io $ plog Error "Cannot open handle: offline mode"
    Just h'  -> do
           io . hPrintf h' "%s\r\n" . IRC.encode $ IRC.privmsg to str
           return ()


-- | Runs a process, returning the output and data from stderr
runProc :: FilePath                 -- ^ Executable name
        -> [String]                 -- ^ Command line parameters
        -> Maybe String             -- ^ Any input
        -> IPluginT (String,String) -- ^ The command output and output from stderr, respectively
runProc f av str = io (run f av str)

-- | Runs a utility (designed to be OS-aware), arguments are identical to that of 'runProc'
runUtil :: FilePath -> [String] -> Maybe String -> IPluginT (String,String)
#ifndef WIN32
runUtil f av str = io $ run ("util" </> f) av str
#else
runUtil f av str = io $ run ("util" </> (f ++ ".exe")) av str
#endif

-- | Gets help for a command, used by 'Infinity.Plugins.System'
pRunHelp :: String          -- ^ Command name
         -> IPluginT String -- ^ Help
pRunHelp cmd = do
  p <- asks plugs
  return $! runhelp p cmd

-- | Returns a list of help for all commands
pListHelp :: IPluginT [String]
pListHelp = do
  p <- asks plugs
  return $! runlist p

-- $plugactions
-- These are the actions that we can perform on the Config.enabledplugin's 'IModule'
-- list, such as find plugins and run them.

-- | Initializes all plugins. When we start up we are given the list of enabled plugins
-- from Config, and then we run this over them yielding a new list with state if it
-- exists. If no state for a plugin exists on disk, then we simply use use the default
-- state that 'mkplugins' provides, otherwise, we replace that particular element with
-- a version of the state that was saved to disk. Plugin state is saved to disk automatically
-- on every reboot (TODO.)
initplugins :: [IModule] -> IO [IModule]
initplugins = mapM f
    where f plug@(IModule (p,st,_)) = do
            newtmvar <- atomically $ newEmptyTMVar
            return $ IModule (p,st,newtmvar)  -- State we got from disk

-- | Saves state of a single plugin
saveplugin :: IModule -> IO ()
saveplugin (IModule (p,st,_)) = when (serial p) $ putState (show p) st


-- | Searches (via 'findplugin') and runs a plugin, if found.
-- It returns the string to be sent back over the network and an
-- updated list of 'IModule's (as the plugin may update
-- state.
runplugin :: IContext       -- ^ Execution context
          -> [IModule]      -- ^ List of plugins
          -> String         -- ^ Command name
          -> Maybe String   -- ^ Arguments
          -> IO (String,[IModule]) 
runplugin ctx mods cmd av = do
  case (findplugin mods cmd) of
    Left s   -> return (s,mods) -- return err str with same mod list

    Right (mods',x@(IModule (p,st,tmvar))) -> do 
      E.bracket_ (atomically $ putTMVar tmvar True) 
                 (atomically $ takeTMVar tmvar) $ do
                   s <- getState (show p)
                   st' <- case s of
                            Nothing -> return st
                            Just v'  -> return v'
                   y <- timeout 15000000 $ runp p st' tmvar
                   case y of
                     Nothing -> do
                       saveplugin x
                       return $ ("<<terminated>>",x:mods')
                     Just (mod',str) -> do
                       saveplugin mod'
                       return (str,mod':mods')

    where runp p' st' tmvar' = do
            (st'',str) <- runReaderT (runPlugT (exec p' st' cmd av)) ctx
            return (IModule (p',st'',tmvar'),str)


-- | Scans the module list with a string and returns
-- an @Either@ indicating it either couldn't find the module and gives
-- the error string (@Left String@) or it found it and gives you the
-- module back (@Right ModSt@)
findplugin :: [IModule] -- ^ Plugin list, i.e. @(enabledplugins config)@
           -> String    -- ^ Command to look for
           -> Either String ([IModule],IModule)
findplugin mods cmd = let (x,mods') = partition (findp cmd) mods
                     in case x of
                          []     -> Left $ "No plugin with command '"++cmd++"' found"
                          [y]    -> Right (mods',y)
                          (_:_)  -> Left $ "Clashing plugins with command '"++cmd++"', aborting."
    where 
      findp c (IModule (p,_,_)) = c `elem` (cmds p)


-- | Gets a help string for a command
runhelp :: [IModule] -> String -> String
runhelp mods cmd = case (findplugin mods cmd) of
                     Left s                    -> s
                     Right (_,(IModule (p,_,_))) -> help p cmd

-- | Lists all plugins/modules and what they offer
runlist :: [IModule] -> [String]
runlist = map f
    where f (IModule (p,_,_)) = unwords [show p,"provides:",unwords (cmds p)]

-- | Makes an IModule given a data constructor that
-- is an instance of 'Plugin', this is so we can
-- keep Config clean, for the most part. This pairs
-- a constructor with it's initial state, but we
-- will load an already existing state from disk
-- providing it exists.
mkplugin :: (Show p, Plugin p s) => p -> IModule
mkplugin p = IModule (p,pinit p,undefined)

#ifdef STATIC_BUILD
binfo = "Static Build,"
#else
binfo = "Dynamic Build,"
#endif

-- | Bot version
ver :: String
ver = unwords 
      [ "infinity v0.4,"
      , binfo
      , os ++ "-" ++ arch
      , compilerName ++ "-" ++ showVersion (compilerVersion)
      ]

-- convenience
io :: IO a -> IPluginT a
io = liftIO
