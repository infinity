module Infinity.Plugins.StateTest where
import Infinity.Plugins
import Control.Monad
import Data.Binary

data StateTest = StateTest
 deriving (Eq,Show)

data ST = ST String
 deriving (Eq,Show)

instance Binary ST where
    put (ST s) = put (0 :: Word8) >> put s
    get = do t <- getWord8
             liftM ST get

instance Plugin StateTest ST where
    serial _ = True
    pinit  _ = ST "Nil"
    cmds   _ = ["get","put"]

    help _ "get" = "gets data"
    help _ "put" = "puts data"

    exec _ x@(ST s) "get" _        = return (x,s)
    exec _ x        "put" Nothing  = return (x,"I need some data...")
    exec _ _        "put" (Just x) = return (ST x,"Done")
