module Infinity.Plugins.Fortune where
import Infinity.Plugins
import Data.Char

data Fortune = Fortune
 deriving (Eq,Show)

instance Plugin Fortune () where
    pinit _          = ()
    cmds _           = ["fortune"]
    help _ "fortune" = "prints a random fortune"

    exec _ _ "fortune" _ = do
      (o,e) <- runProc "fortune" ["-s"] Nothing
      let s = clean $ if (null e) then o else e
      return ((), s)
          where clean = map (\x -> if isControl x then ' ' else x)
