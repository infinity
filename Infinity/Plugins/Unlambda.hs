module Infinity.Plugins.Unlambda where
import Infinity.Plugins
import System.FilePath
import Data.Char

bin = "unlambda"

data Unlambda = Unlambda
 deriving (Eq,Show)

instance Plugin Unlambda () where
    pinit _           = ()
    cmds _            = ["unlambda"]
    help _ "unlambda" = "evaluates an unlambda expression"

    exec _ _ "unlambda" Nothing  = return ((), "Needs an expression")
    exec _ _ "unlambda" (Just x) = do
      (o,e) <- runUtil bin [] $ Just x
      let s = clean (check o e)
      return ((),s)
          where clean = map (\x -> if isControl x then ' ' else x)
                check o e
                      | (null e && null o) = "Done."
                      | (null o) = e
                      | (null e) = o
