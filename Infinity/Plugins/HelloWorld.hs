module Infinity.Plugins.HelloWorld where
import Infinity.Plugins

data HelloWorld = HelloWorld
 deriving (Eq,Show)

instance Plugin HelloWorld () where
    pinit  _ = ()
    cmds   _ = ["hi","bye"]

    help   _ "hi"  = "says \'hello world!\'"
    help   _ "bye" = "says \'goodbye world!\'"

    exec _ _ "hi"  _ = return ((),"hello world!")
    exec _ _ "bye" _ = return ((),"goodbye world!")
