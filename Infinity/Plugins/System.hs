module Infinity.Plugins.System where
import Infinity.Plugins

data System = System
 deriving (Eq,Show)

instance Plugin System () where
    pinit _          = ()
    cmds  _          = ["help","version","list"]
    help _ "help"    = "@help <cmd>, provides help for a command"
    help _ "version" = "Gives version information"
    help _ "list"    = "Lists all commands"

    exec _ _ "version" _     = return ((),ver)
    exec _ _ "help" Nothing  = return ((),"Need command...")
    exec _ _ "help" (Just x) = pRunHelp x >>= \x' -> return ((),x')
    exec _ _ "list" _        = pListHelp >>= \x   -> return ((),unlines x)
