#!/usr/bin/env runghc
import Distribution.Simple
import qualified Control.Exception as Ex
import qualified Tests.Properties as P
import System.Directory
import System.FilePath
import Control.Monad
import System.Info
import System.Exit
import List

inf  = "infinity"
bins = [inf] ++ 
       ["util" </> "unlambda"]    -- files to remove
dirs = [ "."
       , "Tests"
       , "util"
       , "Infinity"
       , "Infinity" </> "Plugins"] -- directories to clean

main = defaultMainWithHooks $ defaultUserHooks {
      runTests  = tests,
      postBuild = copyInfinity,
      postClean = cleanInfinity
}

tests _ _ _ _ = P.main

copyInfinity _ _ _ _ = do
  copyFile (joinPath ["dist","build","infinity",inf]) inf
  ignore (createDirectory "Log" >> createDirectory "State")

cleanInfinity _ _ _ _ = do
  mapM_ (ignore . removeFile) bins
  mapM_ clean dirs
 where
   clean d   = getDirectoryContents d >>= mapM_ (check d)
   check d f = when (any (`isSuffixOf` f) exts)
                (removeFile (d </> f))
   exts      = ["~",".o",".hi",".o-boot",".hi-boot"]


-- convenience
ignore a = Ex.catch a (\_ -> return ())
