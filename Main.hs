{-# LANGUAGE CPP #-}
module Main where
import Control.Concurrent
import System.Environment
import Control.Monad
import System.Exit
#ifdef STATIC_BUILD
import Infinity
#else
import System.Plugins
#endif

ghcargs = []
       ++ map ("-X"++) ["ExistentialQuantification","GeneralizedNewtypeDeriving","MultiParamTypeClasses","CPP","PatternGuards","FunctionalDependencies"]

-- | Main entry point, if the bot is dynamically built
-- it will load the code appropriately and start, otherwise
-- it will just jump to a static main that we will execute with.
-- If executed with '-offline' it will go into offline mode where 
-- you can just experiment with the plugin interface and whatnot.
main :: IO ()
main = do
  av <- getArgs
  let offline = if (null av) then False 
                 else (if head av == "-offline" then True 
                        else False)
  putStrLn "infinity starting..."
#ifndef STATIC_BUILD
  m     <- makeAll "Infinity.hs" ghcargs
  (mod,imain) <- case m of
                   MakeSuccess _ _ -> do
                     -- apparently there's some sort of limitation in regards to having modules
                     -- that re-export other modules and their symbols with hs-plugins. that is,
                     -- we cannot load Infinity.o and get the symbols from there such as imain,
                     -- instead we have to directly load Infinity/Main.o and get the syms from that
                     -- we run makeAll over Infinity.hs since it keeps the project hierarchy nice,
                     -- and it will cause all these modules to be reloaded anyway.
                     when offline $ do
                          stat <- load_ "Infinity/Main.o" [".","Infinity","Infinity/Plugins"] "offlinemode"
                          case stat of
                            LoadSuccess v m -> m
                            LoadFailure e   -> do 
                                        putStrLn "Couldn't load Infinity.Main.offlinemode:"
                                        mapM_ putStrLn e
                                        exitWith $ ExitFailure 127
                     ldstat <- load_ "Infinity/Main.o" [".","Infinity","Infinity/Plugins"] "imain"
                     case ldstat of
                       LoadSuccess v m -> return (v,m)
                       LoadFailure e -> do
                         putStrLn "Couldn't load Infinity.Main.imain:"
                         mapM_ putStrLn e
                         exitWith $ ExitFailure 127
                   MakeFailure e -> do
                     putStrLn "FATAL: Couldn't compile Infinity.hs:"
                     mapM_ putStrLn e
                     exitWith $ ExitFailure 127

  putStrLn "Compiled & Loaded Infinity.Main.imain..."
  imain mod reboot
#else
  when offline offlinemode
  staticmain
#endif

#ifndef STATIC_BUILD
-- | Dynamic rebooting function
reboot :: Module -> a -> IO ()
reboot mod st = do
  mkstat <- makeAll "Infinity.hs" ghcargs
  case mkstat of
    MakeSuccess _ o -> do
      unloadAll mod
      ldstat <- load_ "Infinity/Main.o" [".","Infinity","Infinity/Plugins"] "imain'"
      case ldstat of
        LoadFailure e -> fatality e
        LoadSuccess v imain' -> do
            putStrLn "REBOOT: Successful recompilation & reloading, rebooting..."
            imain' v reboot st
    MakeFailure e -> fatality e
 where
  fatality errs = do
              putStrLn $ "REBOOT: FATAL: Couldn't reboot thread, err:"
              mapM_ putStrLn errs
#else
-- | Dynamic rebooting function
-- Note: This is the static build so this
-- function does nothing.
reboot :: a -> a -> IO ()
reboot _ _ = return ()
#endif
