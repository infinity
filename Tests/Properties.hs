module Tests.Properties where
import Test.QuickCheck
import Control.Monad
import Text.Printf
import System.Exit
import System.IO
import qualified Data.Set as S
import qualified Data.Map as M
import Infinity.Core

instance Arbitrary Server where
    arbitrary = do
      n      <- choose (1,20)  -- length of strings
      k      <- choose (1,30)  -- random amount of chans/admins
      nick   <- vector n :: Gen [Char]
      real   <- vector n :: Gen [Char]
      pass   <- vector n :: Gen [Char]
      chans  <- mapM (vector :: Int -> Gen [Char]) $ replicate k n
      addr   <- vector n :: Gen [Char]
      port   <- choose (1,9000)
      admins <- mapM (vector :: Int -> Gen [Char]) $ replicate k n
      return $ Server addr port (S.fromList chans) nick pass real (S.fromList admins)

instance Arbitrary Bot where
    arbitrary = do
      n     <- choose (1,20) -- random number of servers
      servs <- vector n :: Gen [Server]
      return $ Bot (M.fromList $ zip servs (repeat stdout)) -- we use stdout so we don't get errs since it always exists

-- | Properties
prop_partc_idempotent s c = (joinchan c (joinchan c s)) == joinchan c s
    where types = (s::Server,c::String)

prop_joinc_idempotent s c = (partchan c (partchan c s)) == partchan c s
    where types = (s::Server,c::String)

-- the below test slightly violates procedure, but we have to do it this way
-- so we can get a more proper test. if we use ==> to check that the random
-- channel name is already a member of the server's randomly generated channels,
-- before running partchan, then joinchan, the input space is too large so chances are it
-- will fail because quickcheck will give up on generation, resulting in failure.
-- on the other hand, without that constraint, we will part a channel, returning
-- the same server (since the generated string probably isn't a part of the generated
-- channels,) then join that channel, which will result in a false negative.
-- therefore, we run this over a server we can make sure that has that channel apart of
-- it, since the above tests prove joinchan is idempotent anyway.
prop_joinpart_inverse s c = (joinchan c (partchan c s')) == s'
    where types = (s::Server,c::String)
          s'    = joinchan c s

prop_partjoin_inverse s c = (partchan c (joinchan c s)) == s
    where types = (s::Server,c::String)

prop_newadmin_idempotent s n = (newadmin n (newadmin n s)) == newadmin n s
    where types = (s::Server,n::String)

prop_deladmin_idempotent s n = (deladmin n (deladmin n s)) == deladmin n s
    where types = (s::Server,n::String)

-- see above (prop_joinpart_inverse) for an explanation of this test
prop_newdel_inverse s n = (newadmin n (deladmin n s')) == s'
    where types = (s::Server,n::String)
          s'    = newadmin n s

prop_delnew_inverse s n = (deladmin n (newadmin n s)) == s
    where types = (s::Server,n::String)

prop_servexists b s = (servexists b' s) == True
    where types = (b::Bot,s::Server)
          b'    = joinserv stdout s b

prop_partserv_idempotent s b = (partserv s (partserv s b)) == partserv s b
    where types = (s::Server,b::Bot)

prop_joinserv_idempotent s b = (joinserv stdout s (joinserv stdout s b)) == joinserv stdout s b
    where types = (s::Server,b::Bot)

prop_partjoinserv_inverse s b = (partserv s (joinserv stdout s b)) == b
    where types = (s::Server,b::Bot)

prop_joinpartserv_inverse s b = (joinserv stdout s (partserv s b')) == b'
    where types = (s::Server,b::Bot)
          b'    = joinserv stdout s b

-- | Invariants table
invariants = [ ("joinchan c (joinchan c s) == joinchan c s",
                quickCheck' prop_joinc_idempotent)
             , ("partchan c (partchan c s) == partchan c s", 
                quickCheck' prop_partc_idempotent)
             , ("partchan c (joinchan c s) == s",
                quickCheck' prop_partjoin_inverse)
             , ("joinchan c (partchan c s) == s",
                quickCheck' prop_joinpart_inverse)
             , ("newadmin n (newadmin n s) == newadmin n s",
                quickCheck' prop_newadmin_idempotent)
             , ("deladmin n (deladmin n s) == deladmin n s",
                quickCheck' prop_deladmin_idempotent)
             , ("newadmin n (deladmin n s) == s",
                quickCheck' prop_newdel_inverse)
             , ("deladmin n (newadmin n s) == s",
                quickCheck' prop_delnew_inverse)
             , ("servexists b s == True",
                quickCheck' prop_servexists)
             , ("partserv s (partserv s b) == b",
                quickCheck' prop_partserv_idempotent)
             , ("joinserv undef s (joinserv undef s b) == b",
                quickCheck' prop_joinserv_idempotent)
             , ("partserv s (joinserv undef s b) == b",
                quickCheck' prop_partjoinserv_inverse)
             , ("joinserv undef s (partserv s b) == b",
                quickCheck' prop_joinpartserv_inverse)
             ]

main = do
  b <- mapM (\(s,t) -> printf "%s:\t\t" s >> t) invariants
  printf "%d tests passed.\n" $ (length . filter id) b 
  if (not . and $ b) then
    printf "Err: Not all tests passed.\n" >> exitWith (ExitFailure (-1))
   else printf "Success, all tests passed.\n"
